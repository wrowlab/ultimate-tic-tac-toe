#include "GameFunctions.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

GameFunctions::GameFunctions()
{
}

GameFunctions::~GameFunctions()
{
}

 void GameFunctions::SetupBoard(int board[][3])
{
    int n = 0;
    int b = 0;

    while (n < 3) {
        while (b < 3) {
            board[n][b] = -1;
            b++;
        }
        b = 0;
        n++;
    }
}

void GameFunctions::OutputBoard(int gameBoard[][3]) {
    int n = 0;
    int b = 0;

    while (n < 3) {
        while (b < 3) {
            cerr << gameBoard[n][b] << " ";
            b++;
        }
        cerr << endl;
        b = 0;
        n++;
    }
}

void GameFunctions::GenerateRandomMove(int gameBoard[][3], int& row, int& col)
{
    row = rand() % 3;
    col = rand() % 3;

    while (gameBoard[row][col] != -1) {
        //cerr << myRow << "," << myCol << " : " << gameBoard[myRow][myCol] << endl;
        col++;
        if (col >= 3) {
            col = 0;
            row++;

            if (row >= 3) {
                row = 0;
            }// if

        }//if

    }// while
} //GenerateRandomMove()

int GameFunctions::GetWinner(int board[][3])
{
    int num = 0;
    int winner = -1;

    while (num <= 2 && winner == -1) {
        winner = CheckHorizontal(board, num);
        num++;
    }

    num = 0;
    while (num <= 2 && winner == -1) {
        winner = CheckVertical(board, num);
        num++;
    }

    if (winner == -1) {
        CheckDiagonal(board);
    }

    return winner;
}

int GameFunctions::CheckHorizontal(int board[][3], int row)
{
    if ((board[row][0] == board[row][1]) && (board[row][0] == board[row][2])) {
        return board[row][0];
    }
    else { return -1; }
}

int GameFunctions::CheckVertical(int board[][3], int col)
{
    if ((board[0][col] == board[1][col]) && (board[0][col] == board[2][col])) {
        return board[0][col];
    }
    else { return -1; }
}

int GameFunctions::CheckDiagonal(int board[][3])
{
    if ((board[0][0] == board[1][1]) && (board[0][0] == board[2][2])) {
        return board[0][0];
    }
    else if ((board[0][2] == board[1][1]) && (board[0][2] == board[2][0])) {
        return board[0][2];
    }
    else
    {
        return -1;
    }
}

