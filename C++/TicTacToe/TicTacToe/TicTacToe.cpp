// TicTacToe.cpp : This file contains the 'main' function. Program execution begins and ends there.


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "GameFunctions.h"

using namespace std;


int main()
{
    int gameBoard[3][3];   
    GameFunctions::SetupBoard(gameBoard);    

    int myRow;
    int myCol;
    srand(69);     

    // game loop
    while (1) {
        int opponentRow;
        int opponentCol;
        cin >> opponentRow >> opponentCol; cin.ignore();
        gameBoard[opponentRow][opponentCol] = 1;
        int validActionCount;
        cin >> validActionCount; cin.ignore();
        for (int i = 0; i < validActionCount; i++) {
            int row;
            int col;
            cin >> row >> col; cin.ignore();
        }      
    
        GameFunctions::OutputBoard(gameBoard);

        GameFunctions::GenerateRandomMove(gameBoard, myRow, myCol);        
        
        gameBoard[myRow][myCol] = 0;

        cout << myRow << " " << myCol << endl;

    }// while
}//main

