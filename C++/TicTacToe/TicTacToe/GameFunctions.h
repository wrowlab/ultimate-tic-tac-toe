#pragma once
class GameFunctions
{

public:
	GameFunctions();
	~GameFunctions();

	static void SetupBoard(int[][3]);
	static void OutputBoard(int[][3]);
	static void GenerateRandomMove(int[][3], int&, int&);
	static int GetWinner(int[][3]);
	static int CheckHorizontal(int[][3], int);
	static int CheckVertical(int[][3], int);
	static int CheckDiagonal(int[][3]);
	

};

